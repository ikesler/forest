﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IKesler.Forest.Benchmarks;

namespace IKesler.Forest
{
    class Runner
    {
        const int RepeatCount = 20;
        const int TreeDeep = 5;
        const int TreeWidth = 3;

        readonly IEnumerable<IBenchmark> _benchmarks = new List<IBenchmark>
        {
            new CteBenchmark("data source=.\\SQLEXPRESS;initial catalog=Forest_CTE;user id=sa;password=1;MultipleActiveResultSets=True;"),
            new HierarchyIdBenchmark("data source=.\\SQLEXPRESS;initial catalog=Forest_HierarchyID;user id=sa;password=1;MultipleActiveResultSets=True;"),
            new LinageBenchmark("data source=.\\SQLEXPRESS;initial catalog=Forest_Linage;user id=sa;password=1;MultipleActiveResultSets=True;"),
            new NestedSetBenchmark("data source=.\\SQLEXPRESS;initial catalog=Forest_NestedSet;user id=sa;password=1;MultipleActiveResultSets=True;")
        };

        readonly StringBuilder _log = new StringBuilder();

        public Runner()
        {
            foreach (var benchmark in _benchmarks)
            {
                benchmark.TreeDepth = TreeDeep;
                benchmark.TreeWidth = TreeWidth;
            }
        }

        public void SelectDescendants()
        {
            foreach (var benchmark in _benchmarks)
            {
                _log.AppendLine("Started " + benchmark.GetType().Name);
                double milliseconds = 0;

                for (var i = 0; i < RepeatCount; ++i)
                {
                    var start = DateTime.Now;
                    benchmark.SelectDescendants("/1/2");
                    var span = DateTime.Now - start;
                    milliseconds += span.TotalMilliseconds;
                }

                var average = milliseconds / RepeatCount;
                _log.AppendFormat("Finished. {0} milliseconds taken.\n", average);
            }
        }

        public void InsertAfter()
        {
            foreach (var benchmark in _benchmarks)
            {
                _log.AppendLine("Started " + benchmark.GetType().Name);
                double milliseconds = 0;

                for (var i = 0; i < RepeatCount; ++i)
                {
                    var start = DateTime.Now;

                    for (var j = 0; j < 100; ++j)
                    {
                        benchmark.InsertAfter("/1/2/3", "/1/2/3/" + i + "/" + j);
                    }

                    var span = DateTime.Now - start;
                    milliseconds += span.TotalMilliseconds;
                }

                var average = milliseconds / RepeatCount;
                _log.AppendFormat("Finished. {0} milliseconds taken.\n", average);
            }
        }

        public void RecreateDb(string name)
        {
            var benchmark = _benchmarks.Single(b => b.GetType().Name == name);
            benchmark.RecreateDbSchemaAndData();
        }

        public void RecreateDb()
        {
            foreach (var benchmark in _benchmarks)
            {
                benchmark.RecreateDbSchemaAndData();
            }
        }

        public string GetLog()
        {
            return _log.ToString();
        }
    }
}
