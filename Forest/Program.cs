﻿using System;

namespace IKesler.Forest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting runner");

            var runner = new Runner();
            runner.RecreateDb();
            //runner.SelectDescendants();
            runner.InsertAfter();

            Console.Write(runner.GetLog());
            Console.WriteLine("Runner finished.");
            Console.ReadLine();
        }
    }
}
