﻿using System;

namespace IKesler.Forest.Benchmarks
{
    class BenchmarkException : Exception
    {
        public BenchmarkException(string message) : base(message) { }
    }
}
