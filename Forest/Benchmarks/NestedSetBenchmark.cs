﻿namespace IKesler.Forest.Benchmarks
{
    class NestedSetBenchmark : BaseBenchmark
    {
        protected override string SelectDescendantsSql
        {
            get
            {
                return @"
                    declare @lft bigint
                    declare @rgt bigint

                    select @lft = Lft, @rgt = Rgt from Revision where Name = @parentName

                    select Name from Revision where Lft > @lft and Rgt < @rgt
                ";
            }
        }

        protected override string InsertNodeSql
        {
            get
            {
                return @"
                    declare @parentRgt bigint

                    select @parentRgt = Rgt from Revision where Id = @parentId
                    --Null if root node. Locate it at (1, 2).
                    set @parentRgt = isnull(@parentRgt, 1)
                    update Revision set Rgt = Rgt + 2 where Rgt >= @parentRgt
                    update Revision set Lft = Lft + 2 where Lft > @parentRgt
                    insert into Revision (Name, Lft, Rgt)
                    values (@name, @parentRgt, @parentRgt + 1)
                ";
            }
        }

        protected override string InsertAfterSql
        {
            get
            {
                return @"
                    declare @parentRgt bigint

                    select @parentRgt = Rgt from Revision where Name = @parentName

                    update Revision set Rgt = Rgt + 2 where Rgt >= @parentRgt
                    update Revision set Lft = Lft + 2 where Lft > @parentRgt
                    insert into Revision (Name, Lft, Rgt)
                    values (@name, @parentRgt, @parentRgt + 1)
                ";
            }
        }

        protected override string CreateSchemaSql
        {
            get
            {
                return @"
                    CREATE TABLE dbo.Revision
                        (
                        Id bigint NOT NULL IDENTITY (1, 1),
                        Name nvarchar(MAX) NULL,
                        Lft bigint NOT NULL,
                        Rgt bigint NOT NULL
                        )
                    ALTER TABLE dbo.Revision ADD CONSTRAINT
                    PK_Revision PRIMARY KEY CLUSTERED 
                    (
                    Id
                    ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                ";
            }
        }

        public NestedSetBenchmark(string connectionString) : base(connectionString) { }
    }
}
