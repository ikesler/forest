﻿namespace IKesler.Forest.Benchmarks
{
    class HierarchyIdBenchmark : BaseBenchmark
    {
        protected override string SelectDescendantsSql
        {
            get
            {
                return @"
                    declare @hierarchy hierarchyid 
                    select @hierarchy = Hierarchy
                    from Revision
                    where Name = @parentName

                    select Name
                    from Revision
                    where Hierarchy.IsDescendantOf(@hierarchy) = 1 and Hierarchy <> @hierarchy
                ";
            }
        }

        protected override string InsertNodeSql
        {
            get
            {
                return @"
                    declare @parent hierarchyid
                    select @parent = Hierarchy
                    from Revision
                    where Id = @parentId

                    select @parent = isnull(@parent, hierarchyid::GetRoot())

                    declare @sibling hierarchyid
                    select @sibling = max(Hierarchy)
                    from Revision
                    where Hierarchy.GetAncestor(1) = @parent

                    insert into Revision (Hierarchy, Name)
                    values (@parent.GetDescendant(@sibling, null), @name)
                ";
            }
        }

        protected override string InsertAfterSql
        {
            get
            {
                return @"
                    declare @parent hierarchyid
                    select @parent = Hierarchy
                    from Revision
                    where Name = @parentName

                    select @parent = isnull(@parent, hierarchyid::GetRoot())

                    insert into Revision (Hierarchy, Name)
                    values (@parent.GetDescendant(null, null), @name)
                ";
            }
        }

        protected override string CreateSchemaSql
        {
            get
            {
                return @"
                    CREATE TABLE [dbo].[Revision](
                        [Id] [bigint] NOT NULL IDENTITY (1, 1),
                        [Hierarchy] [hierarchyid] NOT NULL,
                        [Name] [nvarchar](max) NULL,
                     CONSTRAINT [PK_Revision] PRIMARY KEY CLUSTERED 
                    (
                        [Id] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
                ";
            }
        }

        public HierarchyIdBenchmark(string connectionString) : base(connectionString) { }
    }
}
