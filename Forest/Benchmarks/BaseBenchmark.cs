﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace IKesler.Forest.Benchmarks
{
    abstract class BaseBenchmark : IBenchmark
    {

        #region Constants

        private const string ClearDbSql = @"
            if exists(select 1 from sys.tables where name = 'Revision')
            drop table Revision
        ";

        #endregion

        private readonly SqlConnection _connection;

        protected BaseBenchmark(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        #region IBenchmark implementation

        public int TreeDepth { get; set; }
        public int TreeWidth { get; set; }

        public virtual void RecreateDbSchemaAndData()
        {
            ClearDb();
            CreateSchema();
            FillData();
        }

        public virtual void SelectDescendants(string path)
        {
            var depth = TreeDepth - path.Count(c => c == '/') + 1;
            var count = 0;
            using (var reader = SelectDescendantsReader(path))
            {
                while (reader.Read())
                {
                    var name = reader.GetString(0);
                    if (!name.StartsWith(path))
                    {
                        throw new BenchmarkException("Invalid descendant. Expected descendant of '"
                            + path + "' but was " + name);
                    }
                    ++count;
                }
            }
            var expectedCount = NodesCount(depth, TreeWidth);
            if (count != expectedCount)
            {
                throw new BenchmarkException("Invalid size of result set. Expected "
                    + expectedCount + "; was " + count);
            }
        }

        public virtual void InsertAfter(string parentName, string name)
        {
            QueryNone(InsertAfterSql, new { parentName, name });
        }

        #endregion

        #region Private Methods

        private SqlDataReader SelectDescendantsReader(string parentName)
        {
            return QueryReader(SelectDescendantsSql, new { parentName });
        }

        private void CreateLevel(int level, long? parentId, int count, string path)
        {
            var nextLevel = level - 1;
            for (var i = 1; i <= count; ++i)
            {
                var iPath = path + "/" + i;
                InsertNode(parentId, iPath);

                if (nextLevel >= 0)
                {
                    var id = QueryInt("SELECT @@IDENTITY");
                    CreateLevel(nextLevel, id, count, iPath);
                }
            }
        }

        private static int NodesCount(int depth, int width)
        {
            var result = 0;
            for (var i = 1; i <= depth; ++i)
            {
                result += (int)Math.Pow(width, i);
            }
            return result;
        }

        #endregion

        #region Protected Methods

        protected virtual void CreateSchema()
        {
            QueryNone(CreateSchemaSql);
        }

        protected virtual void FillData()
        {
            CreateLevel(TreeDepth, null, TreeWidth, "");
        }

        protected virtual void ClearDb()
        {
            QueryNone(ClearDbSql);
        }

        protected virtual void InsertNode(long? parentId, string name)
        {
            QueryNone(InsertNodeSql, new { parentId, name });
        }

        #endregion

        #region Abstract Properties

        protected abstract string SelectDescendantsSql { get; }
        protected abstract string InsertNodeSql { get; }
        protected abstract string InsertAfterSql { get; }
        protected abstract string CreateSchemaSql { get; }

        #endregion

        #region SQL Helpers

        protected void QueryNone(string sql, object @params = null)
        {
            var command = GetCommand(sql, @params);
            command.ExecuteNonQuery();
        }

        protected SqlDataReader QueryReader(string sql, object @params = null)
        {
            var command = GetCommand(sql, @params);
            return command.ExecuteReader();
        }

        protected long QueryInt(string sql, object @params = null)
        {
            var command = GetCommand(sql, @params);
            return Convert.ToInt64(command.ExecuteScalar());
        }

        protected SqlCommand GetCommand(string sql, object @params = null)
        {
            var command = new SqlCommand
            {
                CommandText = sql,
                Connection = _connection
            };

            if (@params != null)
            {
                var properties = @params.GetType().GetProperties();
                foreach (var property in properties)
                {
                    var paramName = property.Name;
                    var paramValue = property.GetValue(@params);
                    command.Parameters.AddWithValue("@" + paramName, paramValue ?? DBNull.Value);
                }
            }

            return command;
        }

        #endregion

    }
}
