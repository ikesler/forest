﻿using System;

namespace IKesler.Forest.Benchmarks
{
    interface IBenchmark : IDisposable
    {
        int TreeDepth { get; set; }
        int TreeWidth { get; set; }

        void RecreateDbSchemaAndData();
        void SelectDescendants(string path);
        void InsertAfter(string parentName, string name);
    }
}
