﻿namespace IKesler.Forest.Benchmarks
{
    class LinageBenchmark : BaseBenchmark
    {
        protected override string SelectDescendantsSql
        {
            get
            {
                return @"
                    declare @linage varchar(max)
                    select @linage = Linage from Revision where Name = @parentName

                    declare @id bigint
                    select @id = Id from Revision where Name = @parentName

                    select Name from Revision where Linage like @linage + '/' + cast(@id as varchar) + '%'
                ";
            }
        }

        protected override string InsertNodeSql
        {
            get
            {
                return  @"
                    declare @linage varchar(max)
                    select @linage = Linage from Revision where Id = @parentId

                    insert into Revision(ParentId, Name, Linage)
                    values (@parentId, @name, isnull(@linage, '') + '/' + cast(isnull(@parentId, '') as varchar))
                ";
            }
        }

        protected override string InsertAfterSql
        {
            get
            {
                return @"
                    declare @linage varchar(max)
                    declare @parentId bigint
                    select @linage = Linage, @parentId = Id from Revision where Name = @parentName

                    insert into Revision(ParentId, Name, Linage)
                    values (@parentId, @name, isnull(@linage, '') + '/' + cast(isnull(@parentId, '') as varchar))
                ";
            }
        }

        protected override string CreateSchemaSql
        {
            get
            {
                return @"
                    CREATE TABLE [dbo].[Revision](
                    [Id] [bigint] IDENTITY(1,1) NOT NULL,
                    [ParentId] [bigint] NULL,
                    [Name] [nvarchar](max) NULL,
                    [Linage] [varchar](max) NOT NULL,
                    [Depth] [int] NULL,
                    CONSTRAINT [PK_Revision] PRIMARY KEY CLUSTERED 
                    (
                    [Id] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

                    ALTER TABLE [dbo].[Revision]  WITH CHECK ADD  CONSTRAINT [FK_Revision_Revision] FOREIGN KEY([ParentId])
                    REFERENCES [dbo].[Revision] ([Id])
                    ALTER TABLE [dbo].[Revision] CHECK CONSTRAINT [FK_Revision_Revision]
                ";
            }
        }

        public LinageBenchmark(string connectionString) : base(connectionString) { }
    }
}
