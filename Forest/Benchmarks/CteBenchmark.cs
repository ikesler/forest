﻿namespace IKesler.Forest.Benchmarks
{
    class CteBenchmark : BaseBenchmark
    {
        protected override string SelectDescendantsSql
        {
            get
            {
                return @"
                    with Descendants(Id, Name) as (
                    select Id, name
                    from Revision
                    where Name = @parentName
 
                    union all

                    select r.Id, r.Name
                    from Revision r
                        inner join Descendants d
                        on r.ParentId = d.Id
                    )

                    select Name from Descendants where Name <> @parentName
                ";
            }
        }

        protected override string InsertNodeSql
        {
            get
            {
                return @"
                    insert into Revision(ParentId, Name) values (@parentId, @name)
                ";
            }
        }

        protected override string InsertAfterSql
        {
            get
            {
                return @"
                    declare @parentId bigint
                    select @parentId = Id from Revision where Name = @parentName
                    insert into Revision(ParentId, Name)
                    values (@parentId, @name)
                ";
            }
        }

        protected override string CreateSchemaSql
        {
            get
            {
                return @"
                    CREATE TABLE [dbo].[Revision](
                        [Id] [bigint] IDENTITY(1,1) NOT NULL,
                        [Name] [nvarchar](max) NULL,
                        [ParentId] [bigint] NULL,
                     CONSTRAINT [PK_Revision] PRIMARY KEY CLUSTERED 
                    (
                        [Id] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

                    ALTER TABLE [dbo].[Revision]  WITH CHECK ADD  CONSTRAINT [FK_Revision_Revision] FOREIGN KEY([ParentId])
                    REFERENCES [dbo].[Revision] ([Id])

                    ALTER TABLE [dbo].[Revision] CHECK CONSTRAINT [FK_Revision_Revision]
                ";
            }
        }

        public CteBenchmark(string connectionString) : base(connectionString) { }
    }
}
